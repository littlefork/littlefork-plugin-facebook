# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.0"></a>
# 0.1.0 (2017-09-23)


### Features

* Fetch Facebook users. ([2483921](https://gitlab.com/littlefork/littlefork-plugin-facebook/commit/2483921))
* Fetch pages using the api. ([763c693](https://gitlab.com/littlefork/littlefork-plugin-facebook/commit/763c693))
